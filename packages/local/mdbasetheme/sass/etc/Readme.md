# mdbasetheme/sass/etc

This folder contains miscellaneous SASS files. Unlike `"mdbasetheme/sass/etc"`, these files
need to be used explicitly.
