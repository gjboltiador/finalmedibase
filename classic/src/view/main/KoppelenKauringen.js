/**
 * This view is an example list of people.
 */
Ext.define('Mdbase.view.main.KoppelenKeuringen', {
    extend: 'Ext.grid.Panel',
    xtype: 'KoppelenKeuringen',
    style: 'padding: 10px 10px;',
    scrollable: {
    y: 'scroll' },
    requires: [
        'Mdbase.store.Personnel',



    ],
    title: 'Koppelen keuringen en aanpassen prijs',
    id: 'koppelen_keuringen_grid',
    store: {
        type: 'personnel'
    },


    columns: [
        { text: 'Afspraak type',  dataIndex: 'name',flex: 2 },
        { text: 'Duur', dataIndex: 'email', flex: 1 },
        { text: 'Prijs', dataIndex: 'phone', flex: 1 }

    ],
    dockedItems: [
    {
        xtype: 'toolbar',
        flex: 1,
        dock: 'bottom',
        overflowX: 'scroll',
        setWrap: true,
        ui: 'footer',
        id:'koppelen_toolbar',
        layout: {
            pack: 'center',
            type: 'hbox'
        },
        items: [
          {
          ui: 'default-toolbar',
          xtype: 'button',
          cls: 'custom_red_btn',
          overCls: 'custom_red_btn_over',
          text: 'Vragenlijst'
          },
          {
          ui: 'default-toolbar',
          xtype: 'button',
          cls: 'custom_red_btn',
          overCls: 'custom_red_btn_over',
          text: 'Aanpassen medische procedure'
          },
          {
          ui: 'default-toolbar',
          xtype: 'button',
          cls: 'custom_red_btn',
          overCls: 'custom_red_btn_over',
          text: 'E-mail bevestiging'
          },
          {
          xtype:'tbspacer',
          flex:1
          },
          {
          ui: 'default-toolbar',
          xtype: 'button',
          cls: 'custom_red_btn',
          overCls: 'custom_red_btn_over',
          text: 'Toevoegen'
          },
          {
          ui: 'default-toolbar',
          xtype: 'button',
          cls: 'custom_red_btn',
          overCls: 'custom_red_btn_over',
          text: 'Bewerken'
          }
          ,
          {
          ui: 'default-toolbar',
          xtype: 'button',
          cls: 'custom_red_btn',
          overCls: 'custom_red_btn_over',
          text: 'Verwijder'
          }
          ,
          {
          ui: 'default-toolbar',
          xtype: 'button',
          cls: 'custom_red_btn',
          overCls: 'custom_red_btn_over',
          text: 'Afdrukken'
          }
          ,
          {
          ui: 'default-toolbar',
          xtype: 'button',
          cls: 'custom_red_btn',
          overCls: 'custom_red_btn_over',
          text: 'Opslaan'
          }
          ,
          {
          ui: 'default-toolbar',
          xtype: 'button',
          cls: 'custom_red_btn',
          overCls: 'custom_red_btn_over',
          text: 'Sluiten'
          }
        ]
    }
],

    listeners: {
        select: 'onItemSelected',
        documentsave: 'onDocumentSave',
        beforedocumentsave: 'onBeforeDocumentSave',
        dataready: 'onDataReady'
    },

});
