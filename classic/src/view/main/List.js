/**
 * This view is an example list of people.
 */
Ext.define('Mdbase.view.main.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'mainlist',
    style: 'padding: 10px 10px;',
    scrollable: {
    y: 'scroll' },
    requires: [
        'Mdbase.store.Personnel',
        'Ext.toolbar.Paging',


    ],
    title: 'Active Bedrijven',
    id: 'actieve_bedrijven_grid',
    store: {
        type: 'personnel'
    },


    columns: [
        { text: 'Actief sinds',  dataIndex: 'name' },
        { text: 'Bedrijfsnaam', dataIndex: 'email', flex: 1 },
        { text: 'Contactperson', dataIndex: 'phone', flex: 1 },
        { text: 'Telefoonnummer', dataIndex: 'phone', flex: 1 },
        { text: 'Plaats', dataIndex: 'phone', flex: 1 }
    ],

    bbar: {
        xtype: 'pagingtoolbar',
        displayInfo: true,
        displayMsg: 'Gegevens weergeven {0} - {1} of {2}',
        emptyMsg: "No topics to display",


    },
    header: {
        itemPosition: 1, // after title before collapse tool
        items: [{ //export has no plugin yet -- needs premium
            ui: 'default-toolbar',
            xtype: 'button',
            overCls: 'custom_grey_btn_over',
            text: 'Afdrukken',
        },{ //export has no plugin yet -- needs premium
            ui: 'default-toolbar',
            xtype: 'button',
            overCls: 'custom_grey_btn_over',
            text: 'Exporteren naar Excel',
        }]
    },
    dockedItems: [
    {
        xtype: 'toolbar',
        flex: 1,
        dock: 'bottom',
        ui: 'footer',
        layout: {
            pack: 'end',
            type: 'hbox'
        },
        items: [
          {
          ui: 'default-toolbar',
          xtype: 'button',
          iconCls: 'x-fa fa-file',
          overCls: 'custom_grey_btn_over',
          text: 'Nieuw'
          },
          {
          ui: 'default-toolbar',
          xtype: 'button',
          iconCls: 'x-fa fa-edit',
          overCls: 'custom_grey_btn_over',
          text: 'Bewerken'
          },
          {
          ui: 'default-toolbar',
          xtype: 'button',
          iconCls: 'x-fa fa-trash',
          overCls: 'custom_grey_btn_over',
          text: 'Verwijderen'
          },
          {
          ui: 'default-toolbar',
          xtype: 'button',
          iconCls: 'x-fa fa-print',
          overCls: 'custom_grey_btn_over',
          text: 'Afdrukken'
          }
        ]
    }
],

    listeners: {
        select: 'onItemSelected',
        documentsave: 'onDocumentSave',
        beforedocumentsave: 'onBeforeDocumentSave',
        dataready: 'onDataReady'
    },

});
