
Ext.define('Mdbase.view.main.CompanyLoginDetails', {
    extend: 'Ext.Container',
    xtype: 'companyLoginDetails',
    style: 'padding: 10px 10px 10px 20px;',

    items: [{
        xtype: 'button',
        text: 'open Window',


        handler: function() {
            winBillingAddress = Ext.create('Ext.window.Window', {
                modal: true,
                height: 500,
                width: 1070,
                bodyPadding: 10,
                constrain: true,
                closable: true,
                //resizable: false,
                //renderTo: Ext.getBody(),  //what is the use?
                header: {
                    cls: 'header-cls',
                    title: {
                        cls: 'header-title-cls',
                        text: 'Inloggegevens bedrijf'
                    }
                },
                items:[{
                    xtype:'container',
                    layout: 'form',
                    width: 1040,
                    height: 280,
                    bodyStyle: 'margin-bottom: 20px;',
                    items: [{
                        xtype: 'panel',
                        layout: 'vbox',
                        width: 1040,
                        height: 280,

                        style: ' padding: 0px 10px 10px 10px; ',
                        items: [{
                            xtype:'container',
                            layout:'column',
                            width: 1000,
                            items:[{
                                flex:2,
                                xtype: 'label',
                                text: 'Medimark',
                                style: ' margin-top: 30px; font:  36px Century Gothic !important;'
                            },
                                {
                                    flex: 1,
                                    xtype: 'image',
                                    src: 'resources/images/logo.jpg',
                                    style: '  float: right; width: 25%; margin-left: auto; margin-right: auto; margin-top: 0px;',
                                }]
                        },
                            {
                                xtype: 'panel',
                                layout: 'column',
                                width: 1020,
                                style: 'margin-bottom: 20px',
                                items:[{
                                    flex: 1,
                                    layout: 'vbox',
                                    items:[{
                                        xtype:'label',
                                        text: 'Vestigingsadres',
                                        style: '  font: bold  14px Century Gothic !important;',
                                        },
                                        {
                                            layout: 'hbox',
                                            style: 'padding: 0px 10pxx 10px 10px; margin-right 10px; margin-top: 10px;',
                                            items:[{
                                                xtype: 'textfield',
                                                width: 110,
                                                labelWidth: 55,
                                                fieldLabel: 'Postcode',
                                                fieldStyle: 'background: #DAD8DA;',

                                            },
                                                {
                                                    xtype: 'textfield',
                                                    width: 165,
                                                    labelWidth: 80,
                                                    fieldLabel: 'Huisnummer',
                                                    style: 'margin-left: 10px;',
                                                    fieldStyle: 'background: #DAD8DA;',
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    width: 170,
                                                    labelWidth: 80,
                                                    fieldLabel: 'Toevoeging',
                                                    style: 'margin-left: 10px;',
                                                    fieldStyle: 'background: #DAD8DA;',
                                                }]
                                        },
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'Straat',
                                            style: 'margin-top: 10px;',
                                            labelWidth: 55,
                                            width: 465,
                                            fieldStyle: 'background: #DAD8DA;',
                                        },
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'Plaats',
                                            labelWidth: 55,
                                            width: 465,
                                            style: 'margin-top: 10px;',
                                            fieldStyle: 'background: #DAD8DA;',
                                        },
                                        {
                                            xtype: 'combobox',
                                            fieldLabel: 'Land',
                                            labelWidth: 55,
                                            width: 465,
                                            style: 'margin-top: 10px;',
                                            emptyText: 'Netherland',
                                            fieldStyle: 'background: #DAD8DA;',
                                        }]
                                },

                                {
                                    flex: 1,
                                    layout: 'vbox',
                                    style: 'float: right; margin-left: 10px;',
                                    items:[{
                                        xtype:'label',
                                        text: 'Portal login',
                                        style: '  font: bold  14px Century Gothic !important;'
                                    },
                                        {
                                            xtype: 'gridpanel',
                                            height: 140,
                                            border: true,
                                            style: 'background: #ffffff; margin-top: 5px;',
                                            columnWidth: 1,
                                            columns: [{
                                                text: 'Gebruiker',
                                                dataIndex: 'name',

                                                width: 90,
                                                sortable: true
                                            }, {
                                                text: 'Voornam',
                                                dataIndex: 'email',

                                                width: 80,
                                                sortable: true
                                            }, {
                                                text: 'Achternaam',
                                                dataIndex: 'phone',

                                                width: 110,
                                                sortable: true,
                                                renderer: 'renderChange'
                                            }, {
                                                text: 'Initialen',
                                                dataIndex: 'name',

                                                width: 75,
                                                sortable: true,
                                                renderer: 'renderPercent'
                                            }, {
                                                text: 'Verander wachtwoord',
                                                dataIndex: 'email',

                                                width: 170,
                                                sortable: true,
                                                formatter: 'date("m/d/Y")'
                                            }]
                                        }]
                                }]
                            },
                        ]

                    }]
                },
                    {
                        xtype: 'label',
                        text: 'Extra informatie',
                        style: 'margin-top: 40px; margin-left: 20px;',
                    },
                    {

                        xtype: 'textareafield',
                        maxRows: 6,
                        width: 1010,
                        style: 'margin-left: 20px; margin-top: 10px;',
                        fieldStyle: 'background: #DAD8DA;',
                        name: 'extraInfo'

                    },
                    {
                        xtype: 'panel',
                        layout: 'form',
                        flex: 1,
                        style: 'padding: 0px 10px 10px 0px;  margin-left: 380px;  margin-top: 15px; ',


                        items: [{
                            xtype: 'button',
                            text: 'Opslaan',
                            style: ' background: #0a8cff !important; border-radius: 20px; border: 0;  padding: 7px 15px 7px 15px; '
                        },
                        {
                                xtype: 'button',
                                text: 'Afdrukken',
                            style: ' background: #00c853 !important; border-radius: 20px; border: 0;  padding: 7px 15px 7px 15px; margin-left: 15px;'
                        },
                        {
                                xtype: 'button',
                                text: 'Sluiten',
                            style: ' background: #e53238 !important; border-radius: 20px; border: 0;  padding: 7px 15px 7px 15px; margin-left: 15px;'
                        }]
                    }]
            });
            winBillingAddress.show();
        }
    }]
});