/**
 * This view is an example list of people.
 */
Ext.define('Mdbase.view.main.List2', {
    extend: 'Ext.grid.Panel',
    xtype: 'mainlist2',

    requires: [
        'Mdbase.store.Personnel'
    ],

    title: 'Personal Cuteness',

    store: {
        type: 'personnel'
    },

    columns: [
        { text: 'Name',  dataIndex: 'name' },
        { text: 'Email', dataIndex: 'email', flex: 1 },
        { text: 'Phonepal', dataIndex: 'phone', flex: 1 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
