
Ext.define('Mdbase.view.main.AdjustMedicalProcedure', {
    extend: 'Ext.Container',
    xtype: 'adjustMedicalProcedure',
    style: 'padding: 10px 10px 10px 20px;',


    items: [{
                xtype: 'button',
                text: 'open Window',


        handler: function() {
            winMedicalProcedure = Ext.create('Ext.window.Window', {
                modal: true,
                height: 600,
                width: 800,
                bodyPadding: 10,
                constrain: true,
                closable: true,
                resizable: false,
                //renderTo: Ext.getBody(),  //what is the use?
                header:{
                    cls: 'header-cls',
                    title : {
                        cls : 'header-title-cls',
                        text : 'Medische procedure per Afspraaktype'
                    }
                },

                requires: [
                    'Mdbase.store.Personnel'
                ],


                store: {
                    type: 'personnel'
                },

                items:[{
                        xtype: 'container',
                        defaults: {
                            style: 'padding: 0px 10px 10px 0px;  '
                        },
                        items: [{
                            xtype: 'panel',
                            layout: 'vbox',
                            width: 770,
                            heigth: 640,
                           bodyStyle: 'background: #DAD8DA; padding: 0px 10px 10px 10px; ',


                            items:[{
                                xtype: 'combobox',
                                width: 700,
                                reference: 'states',
                                publishes: 'value',
                                fieldLabel: 'Afspraak type:',
                                labelWidth: 220,
                                displayField: 'state',
                                style: 'float: right;  margin-right: 0px; margin-top: 10px; margin-left: 30px;',
                                fieldStyle:'color: #e53238; background: #ffffff; border: none;',
                                anchor: '100%',
                            },
                            {
                                xtype: 'textfield',
                                width: 700,
                                labelWidth: 220,
                                fieldLabel: 'Duur:',
                                style:'float: right; margin-left: 30px; margin-top: 5px;  ',
                                fieldStyle: 'horizontal-align: right; margin-left: 0px; color: #e53238; background: #ffffff; border: none;',
                                anchor: '100%',

                            },

                            {
                               layout:'column',
                                bodyStyle: 'background: #DAD8DA;',
                                items:[{
                                   columnWidth: .85,
                                    layout: 'hbox',
                                    bodyStyle: 'background: #DAD8DA;',
                                    items:[{
                                        xtype: 'combobox',
                                        width: 600,
                                        labelWidth: 220,
                                        reference: 'states',
                                        publishes: 'value',
                                        fieldLabel: 'Kopieer medische procedure van:',
                                        fieldStyle: 'horizontal-align: right; margin-left: 0px; color: #e53238; background: #ffffff; border: none;',
                                        displayField: 'state',
                                        emptyText: 'Selecteer een afspraaktype om te kopieren',
                                        style: 'float: right; margin-top: 5px; margin-right: 15px; background: #DAD8DA; margin-left: 30px;',
                                        anchor: '100%',


                                    }]
                                    },{
                                    columnWidth: .15,
                                    layout:'hbox',
                                    bodyStyle: 'background: #DAD8DA;',
                                    items: [{
                                        xtype: 'button',
                                        text: 'Kopleer',
                                      style:'  background: #e53238 !important; padding: 4px 15px 4px 15px; border: 0px;  border-radius: 0px; margin: 5px 30px 5px 0px;'
                                    }]
                                }]
                            },
                            {
                                layout: 'hbox',
                                width: '100%',
                                style: 'margin-top: 20px;',

                                items: [
                                    {
                                    xtype: 'button',
                                        ui: 'importButton',
                                        text: 'Importeren medische artikelen',
                                     cls: 'btn-text',
                                        iconCls: 'fas fa-file-import',

                                        flex: 1,
                                        style:' color: #404040 !important;  background: #ffffff !important;  border: 0px;  border-radius: 0px; '
                                },
                                    {
                                        xtype: 'button',
                                       // cls: 'btn-text',
                                        text: 'Importeren medische groep',
                                        ui: 'importButton',
                                        iconCls: 'x-fa fa-print',
                                        flex: 1,
                                        style:' color: #404040 !important;  background: #ffffff !important;  border: 0px;  border-radius: 0px; '
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Totale prijs: 300',
                                        //cls: 'btn-text',
                                        iconCls: 'x-fa fa-print',
                                        ui: 'importButton',
                                        style:' color: #404040 !important;  background: #ffffff !important;  border: 0px;  border-radius: 0px; ',
                                        disabled: true,
                                        flex: 1
                                    }]
                            },
                            {
                                xtype: 'gridpanel',
                                height: 340,
                                scrollable: true,
                                border: true,
                                bodyStyle: 'background: #ffffff;',
                                columnWidth: 0.65,
                                columns: [{
                                    text: 'volgnum',
                                    dataIndex: 'name',

                                    width: 100,
                                    sortable: true
                                }, {
                                    text: 'Uitvoeren door',
                                    dataIndex: 'email',

                                    width: 153,
                                    sortable: true
                                }, {
                                    text: 'Naam medische procedure',
                                    dataIndex: 'phone',

                                    width: 240,
                                    sortable: true,
                                    renderer: 'renderChange'
                                }, {
                                    text: 'Prijs',
                                    dataIndex: 'name',

                                    width: 120,
                                    sortable: true,
                                    renderer: 'renderPercent'
                                }, {
                                    text: 'BTW-nummer',
                                    dataIndex: 'email',

                                    width: 125,
                                    sortable: true,
                                    formatter: 'date("m/d/Y")'
                                }]
                            }]
                        },
                            {
                            xtype: 'button',
                            text: 'Sluiten',
                            style: 'background: #e53238; float: right; border-radius: 0px; margin-left: 5px; padding: 4px 5px 4px 5px; margin-right: 10px;'
                        },
                            {
                                xtype: 'button',
                                text: 'Afdrukken',
                                style: 'background: #e53238; float: right;border-radius: 0px; margin-left: 5px; padding: 4px 5px 4px 5px;'
                            },
                            {
                                xtype: 'button',
                                text: 'Verwijderen',
                                style: 'background: #e53238; float: right;border-radius: 0px; margin-left: 5px; padding: 4px 5px 4px 5px;'
                            },
                            {
                                xtype: 'button',
                                text: 'Bewerken',
                                style: 'background: #e53238; float: right;border-radius: 0px; margin-left: 5px; padding: 4px 5px 4px 5px;'
                            },
                            {
                                xtype: 'button',
                                text: 'Nieuw',
                                style: 'background: #e53238; float: right;border-radius: 0px; margin-left: 10px; padding: 4px 5px 4px 5px;'
                            }]
                    }]
            });
            winMedicalProcedure.show();
        }
    }]
});