Ext.define('Mdbase.view.main.BasicWindow', {
    extend: 'Ext.container.Container',
    xtype: 'basic-window',
    style:'background-color: green;',



        defaults: {
            width: 400,
            height: 250,
            bodyPadding: 10,
            style:'background-color: red;',
            autoShow: true
        },

        items: [{
            xtype: 'window',
            title: 'Constraining Window',
            closable: false,
            collapsible: true,
            x: 10, y: 10,
            defaults: {
                width: 200,
                height: 100,
                autoShow: true
            }
        }]
    });


