
Ext.define('Mdbase.view.main.ActiveCompanies', {
    extend: 'Ext.grid.Panel',
    xtype: 'activeCompanies',



        title: 'Browse Forums',
        width: 760,
        height: 500,

        autoLoad: true,
        frame: true,
        disableSelection: true,
        loadMask: true,


        columns: [{
            text: "Topic",
            dataIndex: 'title',

            flex: 1,
            sortable: false,
            renderer: 'renderTopic'
        },{
            text: "Author",
            dataIndex: 'username',

            width: 100,
            hidden: true,
            sortable: true
        },{
            text: "Replies",
            dataIndex: 'replycount',

            width: 70,
            align: 'right',
            sortable: true
        },{
            text: "Last Post",
            dataIndex: 'lastpost',

            width: 150,
            sortable: true,
            renderer: 'renderLast'
        }],

        bbar: {
            xtype: 'pagingtoolbar',
            displayInfo: true,
            displayMsg: 'Displaying topics {0} - {1} of {2}',
            emptyMsg: "No topics to display",

            items: ['-', {
                bind: '{expanded ? "Hide Preview" : "Show Preview"}',
                pressed: '{expanded}',
                enableToggle: true,
                toggleHandler: 'onToggleExpanded'
            }]
        }
    });
