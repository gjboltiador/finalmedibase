Ext.define('Mdbase.view.dashboard.Dashboard', {
    extend: 'Ext.container.Viewport', // #1

    xtype: 'dashboard',
    requires: [
        'Ext.tree.Panel',
        'Ext.list.Tree',
        'Ext.container.Viewport'
    ],
    controller: 'dashboardController',
    viewModel: 'dashboardModel',



    // cls: 'sencha-dash-viewport', ///removed no display
    //itemId: 'dashboardView',
    /*  listeners: {
          render: 'onMainViewRender'
      },*/
      layout: 'border',


      items: [{
       region: 'west',
       id: 'main_treelist_container',
       width: 400,
       split: false,
       //performCollapse: false,
       //collapsible: true,
       reference: 'treelistContainer',
           layout: {
           type: 'vbox',
           align: 'stretch'
       },
       border: false,
       scrollable: 'y',
       items: [{
          xtype: 'panel',
          reference: 'treelist_logo',
          id: 'treelist_logo'
,          border: false,
          height: 90,
          bodyStyle: {
          'background': 'url(resources/images/logo_dashboard.jpg) !important;background-repeat: no-repeat!important'
           }
       },{
           xtype: 'treelist',
           reference: 'treelist',
           bind: '{navItems}',
           listeners: {
               selectionchange: 'onNavigationTreeSelectionChange'
           }
       }]
   }, {
          region:'west',
          id: 'micro_btn_container',
      items: [{
      xtype: 'button',
      text: 'Nav',
      enableToggle: true,
      reference: 'navBtn',
      toggleHandler: 'onToggleNav',
      style: 'display: none'
  },{
        xtype: 'button',
        id:'micro_btn',
        reference: 'icon_right_arrow',
        style:'margin-top:1500%; margin-left: -3px',
        border: false,
        overCls: 'custom_grey_btn_over',
        iconCls:'fa fa-angle-left',
        enableToggle: true,
        toggleHandler: 'onToggleMicro'
      }]

    },{
      xtype: 'container', // #7
      region: 'center',
      bodyPadding: 0,
      style: 'border: 0px; display: block; background-color: #d7d7d7;',
      reference: 'mainCardPanel',
      layout: 'card'
    }]





});
