Ext.define('Mdbase.view.activeBedrijven.ActiveBedrijven', {
    extend: 'Ext.grid.Panel',
    xtype: 'activeCompany',
    style: 'padding: 10px 10px;',


    width: 600,
    height: 400,
    frame: true,
    defaults: {
        bodyPadding: 10,
        scrollable: true,
        layout: {
            type: 'column',
            columnCount: 2
        },
        defaults: {
            columnWidth: 0.5
        }
    },
    items: [{
        title: 'Non lazy tab',
        items: [{
            fieldLabel: 'Field 1',
            xtype: 'textfield',
            style: 'margin: 0 10px 5px 0'
        }, {
            fieldLabel: 'Field 2',
            xtype: 'numberfield'
        }, {
            fieldLabel: 'Field 3',
            xtype: 'combobox',
            style: 'margin: 0 10px 5px 0'
        }, {
            fieldLabel: 'Field 4',
            xtype: 'datefield'
        }]
    }, {
        title: 'Lazy Tab',
        plugins: {
            lazyitems: {
                items: [{
                    fieldLabel: 'Field 1',
                    xtype: 'textfield',
                    style: 'margin: 0 10px 5px 0'
                }, {
                    fieldLabel: 'Field 2',
                    xtype: 'numberfield'
                }, {
                    fieldLabel: 'Field 3',
                    xtype: 'combobox',
                    style: 'margin: 0 10px 5px 0'
                }, {
                    fieldLabel: 'Field 4',
                    xtype: 'datefield'
                }]
            }
        }
    }]
});