Ext.define('Mdbase.view.login.Login', {
    extend: 'Ext.container.Viewport',
    xtype: 'login',
    fullscreen: true,
    layout: 'center',

    requires: [
        'Mdbase.view.login.LoginController',
        //'Ext.panel.Panel'
    ],

    controller: 'login',
    autoShow: true,
    style: 'background-color: White;',

    items: [{
      xtype: 'panel',   //Main Panel No Border
      frame:false,
      height: 600,
      width: 500,
      bodyPadding: 0,
      style: 'border: 0px; display: block; margin-left: auto; margin-right: auto; margin-top: 0px;',



      items: [ {
          xtype: 'image',
          src: 'resources/images/logo.jpg',
          flex: 1,
          style: 'width: 100%; display: block; margin-left: auto; margin-right: auto; margin-top: 0px;',
      },
          {
              xtype: 'panel',  //Sub-Panel with border red

              frame:true,
              height: 325,
              width: 500,
              bodyPadding: 30,
              style: 'border: solid #e53238 3px; display: block; margin-left: auto; margin-right: auto; margin-top: 0px;',


              items: [{
                  xtype: 'label',
                  text: 'Sign In',
                  style: 'font: normal 24px arial !important; color: #e53238; margin-bottom: 40px; margin-left: 50px; '
              },

                  {
                      xtype: 'panel',
                      name: 'credentials',

                      style: 'border: 0px; display: block; margin-left: 50px; margin-right: auto; margin-top: 40px; background-color: white;',
                      defaults: {
                          width: 360,
                          fieldStyle: 'border-radius: 20px; background-color: #e53238; background-image: none; color: white; padding: 7px 15px 7px 15px;',
                          cls: 'credentials'
                      },
                      items:[
                          {
                              xtype: 'textfield',
                              allowBlank: false,
                              name: 'user',
                              emptyText: 'Username',
                              msgTarget: 'qtips',
                              inputWrapCls: '',
                              triggerWrapCls: ''
                          },{
                              height:40
                          },
                          {
                              xtype: 'textfield',
                              allowBlank: false,
                              name: 'pass',
                              emptyText: 'Password',
                              inputType: 'password',
                              msgTarget: 'qtip',
                              inputWrapCls: '',
                              triggerWrapCls: ''
                          },
                          {
                              xtype: 'button',
                              itemId: 'login',
                              id: 'login',
                              text: 'LOGIN',
                              width: 100,
                              cls: 'loginBtn',

                              formBind: true,
                              listeners: {
                                  click: 'onLoginClick'
                              }
                          }
                      ]

                  } //End of panel credentials
              ]



          },  //End of sub-panel with red border
          {
              xtype: 'container',
              height: 75,
              width: 500,
              layout: {
                  type: 'hbox',
                  align: 'stretch'
              },

              style: 'border: 1px; display: block; margin-left: 0px; margin-right: auto; margin-top: 10px;',

              items: [{
                  width:'55%',
                  style:'float:left',
                  html:"<div>Copyright@ 1998-2020 Medimark<br>All rights reserved</div>",
              },{
                  width:'45%',
                  style:'float:right',
                  html:"<div>Users are advice to read the terms and conditions carefully</div>",
              }
              ]
          }


      ]




  }] //End of Main Panel
});
