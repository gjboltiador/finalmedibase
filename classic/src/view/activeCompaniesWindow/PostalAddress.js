
Ext.define('Mdbase.view.activeCompaniesWindow.PostalAddress', {
    extend: 'Ext.Container',
    xtype: 'postalAddress',
    scrollable: true,
    height: 440,
    items: [{
        xtype: 'container',
        layout: 'vbox',

        scrollable: true,
        items: [{
            xtype:'container',
            layout:'column',
            width: 910,
            items:[{
                flex:2,
                xtype: 'label',
                text: 'Sedgwick',
                style: ' margin-top: 30px; font:  36px Cambria Math !important; margin-left: 40px;'
            },
                {
                    flex: 1,
                    xtype: 'image',
                    src: 'resources/images/sedgwick.png',
                    style: '  float: right; width: 12%; margin-left: auto; margin-right: 20px; margin-top: 0px;',
                }]
        },
            {
                xtype:'container',
                layout:'column',
                width: 920,
                style: '  margin-left: 20px;',
                items:[{
                    xtype: 'container',
                    flex: 1,
                    width: 450,
                    layout: 'vbox',
                    items:[{
                        xtype: 'label',
                        text: 'Hoofdvestigingsadres',
                        style: ' margin-top: 50px; font: bold  14px Century Gothic !important; margin-left: 0px; margin-bottom: 20px;'
                    },

                        {
                            xtype:'container',
                            layout: 'hbox',
                            width: 450,
                            items: [{
                                xtype: 'textfield',
                                width: 165,
                                labelWidth: 110,
                                fieldLabel: 'Postcode'
                            },
                                {
                                    xtype:'textfield',
                                    width: 130,
                                    labelWidth: 75,
                                    fieldLabel: 'Huisnummer',
                                    style: 'margin-left: 5px;'

                                },
                                {
                                    xtype:'textfield',
                                    width: 125,
                                    labelWidth: 70,
                                    fieldLabel: 'Toevoeging',
                                    style: 'margin-left: 5px;'

                                }]
                        },
                        {
                            xtype:'textfield',
                            width: 430,
                            labelWidth: 110,
                            fieldLabel: 'Straat',
                            style: 'margin-left: 0px; margin-top: 10px;'
                        },
                        {
                            xtype:'combobox',
                            width: 430,
                            labelWidth: 110,
                            fieldLabel: 'Land',
                            emptyText: 'Nederland',
                            style: 'margin-left: 0px; margin-top: 10px;'
                        },
                        {
                            xtype:'textfield',
                            width: 430,
                            labelWidth: 110,
                            fieldLabel: 'E-mailadres',
                            style: 'margin-left: 0px; margin-top: 10px;'
                        },
                        {
                            xtype:'textfield',
                            width: 430,
                            labelWidth: 110,
                            fieldLabel: 'Telefoonnummer',
                            style: 'margin-left: 0px; margin-top: 10px;'
                        },
                        {
                            xtype:'textfield',
                            width: 430,
                            labelWidth: 110,
                            fieldLabel: 'Website',
                            style: 'margin-left: 0px; margin-top: 10px;'
                        }]
                },
                    {
                        xtype: 'container',
                        style: '  margin-left: 10px;',
                        flex: 1,
                        width: 450,
                        layout: 'vbox',
                        items:[{
                            xtype:'checkbox',
                            boxLabel: 'Zelfde als hoofdvestigingsadres?',
                            style: ' margin-top: 10px; '
                        },
                            {
                                xtype: 'label',
                                text: 'Nevenvestigingsadres',
                                style: ' margin-top: 11px; font: bold  14px Century Gothic !important; margin-left: 0px; margin-bottom: 20px;'
                            },
                            {
                                xtype:'container',
                                layout: 'hbox',
                                width: 450,
                                items: [{
                                    xtype: 'textfield',
                                    width: 165,
                                    labelWidth: 110,
                                    fieldLabel: 'Postcode'
                                },
                                    {
                                        xtype:'textfield',
                                        width: 130,
                                        labelWidth: 75,
                                        fieldLabel: 'Huisnummer',
                                        style: 'margin-left: 5px;'

                                    },
                                    {
                                        xtype:'textfield',
                                        width: 125,
                                        labelWidth: 70,
                                        fieldLabel: 'Toevoeging',
                                        style: 'margin-left: 5px;'

                                    }]
                            },
                            {
                                xtype:'textfield',
                                width: 430,
                                labelWidth: 110,
                                fieldLabel: 'Straat',
                                style: 'margin-left: 0px; margin-top: 10px;'
                            },
                            {
                                xtype:'combobox',
                                width: 430,
                                labelWidth: 110,
                                fieldLabel: 'Land',
                                emptyText: 'Nederland',
                                style: 'margin-left: 0px; margin-top: 10px;'
                            },
                            {
                                xtype:'textfield',
                                width: 430,
                                labelWidth: 110,
                                fieldLabel: 'E-mailadres',
                                style: 'margin-left: 0px; margin-top: 10px;'
                            },
                             {
                                xtype:'textfield',
                                width: 430,
                                labelWidth: 110,
                                fieldLabel: 'Telefoonnummer',
                                style: 'margin-left: 0px; margin-top: 10px;'
                            },
                            {
                                xtype:'textfield',
                                width: 430,
                                labelWidth: 110,
                                fieldLabel: 'Website',
                                style: 'margin-left: 0px; margin-top: 10px;'
                            }]
                    }]
            },
            {
                xtype: 'label',
                text: 'Extra informatie',
                style: 'margin-top: 20px; margin-left: 20px;',
            },
            {
                xtype: 'textareafield',
                maxRows: 6,
                width: 890,
                style: 'margin-left: 20px; margin-top: 10px; margin-bottom: 20px;',
            }],
    }]

});