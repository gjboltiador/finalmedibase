Ext.define("Mdbase.view.generalInformation.ActiveCompaniesWindow",{
    extend:'Ext.Container',
    xtype: 'activeCompaniesWindow',
    require:[
        "Mdbase.view.activeCompaniesWindow.GeneralInformation"
    ],
    defaults: {
        modal: true,
        height: 600,
        width: 980,
        bodyStyle: 'padding: 0px 20px 2px 10px;',
        constrain: true,
        closable: true,
        resizable: false,
        autoShow: true
    },
   items:[{
        xtype: 'window',
       items:[{
            xtype: 'tabpanel',

           fullscreen: true,
           activeTab: 3, // index or id
           items:[{
               title: 'Algemeneinformatie',
               xtype:"generalInformation"
           },{
               title: 'Vestingsadres',
               xtype: 'locationAddress'
           },{
               title: 'Postadres',
               xtype: 'postalAddress'
           },{
               title: 'Factuuradres',
              xtype: 'billingAddress'
           }]
       },{
           xtype: 'panel',
           layout: 'form',
           flex: 1,
           style: 'padding: 0px 10px 10px 0px;  margin-left: 330px;  margin-top: 15px; ',


           items: [{
               xtype: 'button',
               text: 'Opslaan',
               style: ' background: #0a8cff !important; border-radius: 20px; border: 0;  padding: 7px 15px 7px 15px; '
           },
               {
                   xtype: 'button',
                   text: 'Afdrukken',
                   style: ' background: #00c853 !important; border-radius: 20px; border: 0;  padding: 7px 15px 7px 15px; margin-left: 15px;'
               },
               {
                   xtype: 'button',
                   text: 'Sluiten',
                   style: ' background: #e53238 !important; border-radius: 20px; border: 0;  padding: 7px 15px 7px 15px; margin-left: 15px;'
               }]



       }]

   }]
})
