    Ext.define('Mdbase.view.dashboard.DashboardModel', {
        extend: 'Ext.app.ViewModel',

        alias: 'viewmodel.dashboardModel',

        formulas: {
            selectionText: function(get) {
                var selection = get('treelist.selection'),
                    path;
                if (selection) {
                    path = selection.getPath('text');
                    path = path.replace(/^\/Root/, '');
                    return 'Selected: ' + path;
                } else {
                    return 'No node selected';
                }
            }
        },

        stores: {
            navItems: {
                type: 'tree',
                root: {
                    expanded: true,
                    children: [{
                        text: '\t Welcome DAVID',
                        iconCls: 'x-fa fa-user redIcon',
                        leaf: true
                    },{
                        text: 'Agenda',
                        iconCls: 'x-fa fa-calendar redIcon',
                        leaf: true,
                        route : 'mainlist2',
                        viewType: 'mainlist2'

                    },{
                        text: 'Contact',
                        iconCls: 'x-fa fa-envelope-o redIcon',
                        leaf: true,
                        route : 'basic-window',
                        viewType: 'basic-window'


                    },{
                        text: 'Manual',
                        iconCls: 'x-fa fa-book redIcon',
                        leaf: true
                    },{
                        text: 'Medicash',
                        iconCls: 'x-fa fa-credit-card redIcon',
                        leaf: true

                    },{
                        text: 'MediMaint',
                        iconCls: 'x-fa fa-wrench redIcon',


                        children: [{
                           text: 'Soort afspraak (Type of appointment)',
                           children: [{
                               text: 'Koppelen keuringen en aanpassen prijs (Linking inspections and adjusting price) ',
                               children: [{
                                   text: 'Aanpassen medische procedure (Adjust medical procedure)',
                                   leaf: true,
                                   route : 'adjustMedicalProcedure',
                                   viewType: 'adjustMedicalProcedure'
                               }]
                           },{
                               text: 'Vragenlijst (Questionnaire)',
                               leaf: true
                           },{
                               text: 'Toewijzen bedrijf (Assign company)',
                               children: [{
                                   text: 'Weergeven toegewezen bedrijf (View assigned company)',
                                   leaf: true
                               }]
                           }]
                       },{
                           text: 'Gegevens klanten (Customer data)',
                           children: [{
                               text: 'Bedrijven (Companies)',
                               children: [{
                                   text: 'Actieve bedrijven (Active companies)s',
                                   route : 'mainlist',
                                   viewType: 'mainlist',

                                   children: [{
                                            text: 'Inloggegevens bedrijven (Company login details)',
                                            leaf: true,
                                       route : 'companyLoginDetails',
                                       viewType: 'companyLoginDetails',
                                   },{
                                       text: 'Algemene informatie (General information)',
                                       route : 'activeCompaniesWindow',
                                       viewType: 'activeCompaniesWindow',
                                       children: [{
                                           text: 'Vestigingsadres (Location address)',
                                           leaf:true
                                       },{
                                           text: 'Postadres (Postal address)',
                                           leaf: true
                                       },{
                                           text: 'Factuuradres (Billing address)',
                                           leaf: true,
                                           route : 'billingAddress',
                                           viewType: 'billingAddress',

                                       },{
                                           text: 'Koppelen keuringen en aanpassen prijs (Linking inspections and adjusting price)',
                                           route: 'KoppelenKeuringen',
                                           viewType: 'KoppelenKeuringen',
                                           children:[{
                                               text: 'Aanpassen medische procedure (Adjust medical procedure)',
                                               leaf: true
                                           },{
                                               text: 'Vragenlijst (Questionnaire)',
                                               leaf: true
                                           }]
                                       }]
                                   }]
                               },{
                                   text: 'Code inlog medewerkers (Code login employees)',
                                   leaf: true
                               },{
                                    text: 'Niet actieve bedrijven (Not active companies)',
                                   children: [{
                                        text: 'Inloggegevens bedrijven (Company login details)',
                                       leaf: true
                                   },{
                                        text: 'Algemene informatie (General information)',
                                       children:[{
                                            text: 'Vestigingsadres (Location address)',
                                           leaf: true
                                       },{
                                            text: 'Postadres (Postal address)',
                                           leaf:  true
                                       },{
                                            text: 'Factuuradres (Billing address)',
                                           leaf: true
                                       },{
                                           text: 'Koppelen keuringen en aanpassen prijs (Linking inspections and adjusting price)',
                                           children:[{
                                               text: 'Aanpassen medische procedure (Adjust medical procedure)',
                                               leaf: true
                                           },{
                                               text: 'Vragenlijst (Questionnaire)',
                                               leaf: true
                                           }]
                                       }]
                                   }]
                               }]

                           },{
                               text: 'Particulieren (Individuals)',
                               children:[{
                                   text: 'Cliënt informatie (Client information) ',
                                   children:[{
                                       text: 'Cliënt geschiedenis (Client history)',
                                       children:[{
                                           text: 'Soort afspraak particulier (Type of appointment private)',
                                           leaf: true
                                       },{
                                           text: 'Soort afspraak bedrijf (Type of appointment company)',
                                           leaf:true
                                       }]
                                   }]
                               }]

                           },{
                               text: 'Verzekeraars (Insurance)',
                               children: [{
                                   text: 'Actieve bedrijven (Active companies)',
                                   children: [{
                                       text:'Inloggegevens bedrijven (Company login details)',
                                       leaf: true
                                   },{
                                       text: 'Algemene informatie (General information)',
                                       children: [{
                                           text:'Vestigingsadres (Location address)',
                                           leaf: true
                                       },{
                                           text:'Postadres (Postal address)',
                                           leaf: true
                                       },{
                                           text: 'Factuuradres (Billing address)',
                                           leaf: true
                                       },{
                                           text: 'Koppelen keuringen en aanpassen prijs (Linking inspections and adjusting price)',
                                           children:[{
                                               text: 'Aanpassen medische procedure (Adjust medical procedure)',
                                               leaf: true
                                           },{
                                               text: 'Vragenlijst (Questionnaire)',
                                               leaf: true
                                           }]
                                       }]
                                   }]
                               }]
                           },{

                           }]
                       },{
                           text: 'Gebruikers (Users)',
                           children: [{
                               text: 'Verander wachtwoord (Change password)',
                               leaf: true
                           }]
                       },{
                            text: 'Laboratorium/Medisch (Laboratory/Medical)',
                            children:[{
                                text: 'Artikelen (Articles)',
                                leaf: true
                            }]
                        },{
                            text: 'Vragenlijst (Questionnaire)',
                            branch: 'true',

                            children:[{
                                text: 'Custom template (Custom template)',
                                leaf: true
                            },{
                                text: 'Per Afspraaktype (Per Appointment)',
                                leaf: true
                            },{
                                text: 'Per bedrijf (Per Company)',
                                leaf: true
                            },{
                                text: 'Vragen en mogelijke antwoorden (Questions and possible answers)',
                                leaf: true
                            }]
                        },{
                            text: 'Taal (Language)',
                            leaf: true
                        },{
                            text: 'Locatie (Location)',
                            leaf: true
                        },{
                            text: 'Instellingen (Settings)',
                            leaf: true
                        }]
                    },{
                        text: 'MediTime',
                        iconCls: 'x-fa fa-clock-o redIcon',
                        leaf: true
                    },{
                        text: 'Statistics',
                        iconCls: 'x-fa fa-line-chart redIcon',
                        leaf: true
                    },{
                        text: 'Settings',
                        iconCls: 'x-fa fa-cog redIcon',
                        leaf: true
                    },{
                        text: 'Logout',
                        iconCls: 'x-fa fa-sign-out redIcon',
                        leaf: true
                    },{
                        text: 'Select a doctor',
                        style: 'margin-top: 20px;',
                        iconCls: 'x-fa fa-user-md redIcon',
                        leaf: true
                    },{
                        text: 'Select Location',
                        iconCls: 'x-fa fa-map-marker redIcon',
                        leaf: true
                    }]
                }
            }
        }
    });
