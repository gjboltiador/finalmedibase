Ext.define('Mdbase.view.dashboard.DashboardController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.dashboardController',

    onToggleConfig: function (menuitem) {
        var treelist = this.lookupReference('treelist');

        treelist.setConfig(menuitem.config, menuitem.checked);
    },

    onToggleMicro: function (button, pressed) {
        var treelist = this.lookupReference('treelist'),
            navBtn = this.lookupReference('navBtn'),
            navlist = this.lookupReference('navlist'), //gf
            icon = this.lookupReference('icon_right_arrow'),
            treelistLogo = this.lookupReference('treelist_logo'),


      //      navct = navlist.ownerCt; //gf
            ct = treelist.ownerCt;


        treelist.setMicro(pressed);

        if (pressed) {
            navBtn.setPressed(true);
            navBtn.disable();
            this.oldWidth = ct.width;
            ct.setWidth(34);
            icon.setIconCls('fa fa-angle-right');
            treelistLogo.addCls('treelist_logo_indent');
            treelistLogo.removeCls('x-box-item');

        //    navct.setWidth(44); //gf
        } else {
            ct.setWidth(this.oldWidth);
          //  navct.setWidth(this.oldWidth); //gf
            navBtn.enable();
            icon.setIconCls('fa fa-angle-left');
            treelistLogo.removeCls('x-box-item, treelist_logo_indent');
        }

        // IE8 has an odd bug with handling font icons in pseudo elements;
        // it will render the icon once and not update it when something
        // like text color is changed via style addition or removal.
        // We have to force icon repaint by adding a style with forced empty
        // pseudo element content, (x-sync-repaint) and removing it back to work
        // around this issue.
        // See this: https://github.com/FortAwesome/Font-Awesome/issues/954
        // and this: https://github.com/twbs/bootstrap/issues/13863
        if (Ext.isIE8) {
            this.repaintList(treelist, pressed);

        }
    },

    onToggleNav: function (button, pressed) {
        var treelist = this.lookupReference('treelist'),
            ct = this.lookupReference('treelistContainer');


        treelist.setUi(pressed ? 'nav' : null);
        treelist.setHighlightPath(pressed);
        ct[pressed ? 'addCls' : 'removeCls']('treelist-with-nav');

        if (Ext.isIE8) {
            this.repaintList(treelist);
        }
    },

    repaintList: function(treelist, microMode) {
        treelist.getStore().getRoot().cascade(function(node) {
            var item, toolElement;

            item = treelist.getItem(node);

            if (item && item.isTreeListItem) {
                if (microMode) {
                    toolElement = item.getToolElement();

                    if (toolElement && toolElement.isVisible(true)) {
                        toolElement.syncRepaint();
                    }
                }
                else {
                    if (item.element.isVisible(true)) {
                        item.iconElement.syncRepaint();
                        item.expanderElement.syncRepaint();
                    }
                }
            }
        });
    },



    //From sir Egee
    listen : {
        controller : {
            '#' : {
                unmatchedroute : 'onRouteChange'
            }
        }
    },

    routes: {
        ':node': 'onRouteChange'
    },

    onRouteChange: function(id) {
        this.setCurrentView(id);
    },

    setCurrentView: function(hashTag) {
        hashTag = (hashTag || '').toLowerCase();

        var me             = this,
            refs           = me.getReferences(),
            mainCard       = refs.mainCardPanel, // reference sa center container
            mainLayout     = mainCard.getLayout(),
            navigationList = refs.treelist, // reference ni sa katong tree store nga view
            store          = navigationList.getStore(),
            node           = store.findNode('route', hashTag) ||
                store.findNode('viewType', hashTag),
            view           = (node && node.get('viewType')) || 'page404',
            lastView       = me.lastView,
            existingItem   = mainCard.down(view),
            newView;

        // Kill any previously routed window
        if (lastView && lastView.isWindow) {
            lastView.destroy();
        }

        lastView = mainLayout.getActiveItem();

        if (!existingItem) {

            newView = Ext.create({
                xtype: view,
                routeId: hashTag, // for existingItem search later
                hideMode: 'offsets'
            });
        } console.log('value', newView );

        if (!newView || !newView.isWindow) {
            // !newView means we have an existing view, but if the newView isWindow
            // we don't add it to the card layout.
            if (existingItem) {
                // We don't have a newView, so activate the existing view.
                if (existingItem !== lastView) {
                    mainLayout.setActiveItem(existingItem);
                }
                newView = existingItem;
            } else {
                // newView is set (did not exist already), so add it and make it the
                // activeItem.
                Ext.suspendLayouts();
                mainLayout.setActiveItem(mainCard.add(newView));
                Ext.resumeLayouts(true);
            }
        }

        navigationList.setSelection(node); // set the selected navigation

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        me.lastView = newView;
    },
    onNavigationTreeSelectionChange : function( treelist, record ) {
        console.log('record', record);
        let hashTag = record && record.get('route');

        if (hashTag && hashTag.length) {
            console.log('>hashtag>', hashTag);
            this.redirectTo( hashTag );   //change url
        }
    }

});
